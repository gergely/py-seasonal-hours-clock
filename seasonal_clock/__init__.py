"""Main module of the Seasonal Clock"""

__version__ = '0.1.0'

from datetime import datetime

from astral import Depression, LocationInfo, moon
from pytz import UTC

from .config import load_config
from .hours import seasonal_hours
from .times import collect_day_parts, get_rahukaalam


def get_moon_phase_text(moon_phase: float) -> str:
    """Get the name of the Moon phase"""

    name = 'Dark Moon'

    if moon_phase < 0.05:
        name = 'New Moon'
    elif moon_phase < 6.95:
        name = 'Waxing Crescent Moon'
    elif moon_phase < 7.05:
        name = 'Waxing Half Moon'
    elif moon_phase < 13.95:
        name = 'Waxing Gibbous Moon'
    elif moon_phase < 14.05:
        name = 'Full Moon'
    elif moon_phase < 20.95:
        name = 'Waning Gibbous Moon'
    elif moon_phase < 21.05:
        name = 'Waning Half Moon'
    elif moon_phase < 27.95:
        name = 'Waning Crescent Moon'

    return name


def main() -> None:
    """The Main Thing™"""

    config = load_config()

    location = LocationInfo(
        config['city'],
        config['country'],
        config['timezone'],
        config['latitude'],
        config['longitude'],
    )
    local_tz = location.tzinfo
    utc_now = datetime.utcnow().replace(tzinfo=UTC)
    local_now = utc_now.astimezone(local_tz)

    moon_phase = moon.phase(local_now)

    day_parts = collect_day_parts(location.observer, local_now)

    final_name = 'Night'
    upcoming_name = None
    final_idx = -1

    for idx, (_, time, name, next_name) in enumerate(day_parts):
        if utc_now > time:
            final_name = name
            upcoming_name = next_name
            final_idx = idx

    next_time = day_parts[final_idx + 1][1]

    if upcoming_name is None:
        upcoming_name = day_parts[0][2]

    print(
        f'Now: {local_now.strftime("%Y-%m-%d %H:%M:%S")} (UTC {utc_now.strftime("%Y-%m-%d %H:%M:%S")})'
    )
    print(f'{final_name}, {next_time-local_now} until {upcoming_name}')

    rahukaalam, next_rahukaalam = get_rahukaalam(location.observer, local_now)

    if next_rahukaalam is not None:
        if rahukaalam:
            print(
                f'Rāhukāla (until {next_rahukaalam.astimezone(local_tz).strftime("%H:%M:%S")})'
            )
        else:
            print(
                f'Next Rāhukāla at {next_rahukaalam.astimezone(local_tz).strftime("%H:%M:%S")}'
            )

    print(get_moon_phase_text(moon_phase))

    hour_num = utc_now.hour
    hour_short = seasonal_hours[utc_now.hour].short
    hour_long = seasonal_hours[utc_now.hour].long
    print(f'{hour_long} ({hour_short}, {hour_num})')
