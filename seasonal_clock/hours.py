"""Hour names for the Seasonal Clock"""


class HourProxy:
    """Proxy class to get short and long hour names"""

    HOUR_NAMES = (
        ('candle', 'candle hour'),
        ('ice', 'hour of ice'),
        ('comet', 'hour of the comet'),
        ('thimble', 'hour of the thimble'),
        ('root', 'hour of roots'),
        ('mist', 'hour of mist'),
        ('sprout', 'sprout hour'),
        ('rainbow', 'rainbow hour'),
        ('worm', 'worm hour'),
        ('bud', 'bud hour'),
        ('blossom', 'blossom hour'),
        ('ladybug', 'ladybug hour'),
        ('geese', 'hour of geese'),
        ('dust', 'hour of dust'),
        ('peach', 'hour of peach'),
        ('fog', 'hour of fog'),
        ('acorn', 'hour of acorn'),
        ('gourd', 'hour of gourd'),
        ('soup', 'soup hour'),
        ('crow', 'crow hour'),
        ('mushroom', 'mushroom hour'),
        ('thunder', 'thunder hour'),
        ('frost', 'frost hour'),
        ('lantern', 'lantern hour'),
    )

    def __init__(self, hour: int) -> None:
        self.hour = hour

    @property
    def short(self) -> str:
        """The short name of the hour"""

        return self.HOUR_NAMES[self.hour][0]

    @property
    def long(self) -> str:
        """The long name of the hour"""

        return self.HOUR_NAMES[self.hour][1]

    def __str__(self) -> str:
        return self.short


class SeasonalHours:  # pylint: disable=too-few-public-methods
    """Class to access the hour names"""

    def __getitem__(self, hour: int) -> HourProxy:
        if not 0 <= hour <= 23:
            raise ValueError(f'Invalid hour {hour}')

        return HourProxy(hour)


seasonal_hours = SeasonalHours()
