Seasonal Clock
##############

Based on Cinnamon’s [React
version](https://github.com/cinnamon-bun/seasonal-hours-clock).

Install
=======

Copy `example-config.toml` to `XDG_CONFIG_HOME/seasonal-clock.toml`
(`XDG_CONFIG_HOME` is usually `$HOME/.config` on Linux systems) and edit it.

Configuration
=============

Everything configurable should be under the `[seasonal-clock]` section.

`country` and `city` are, well, the country and city you live in.  These are
optional and will be sent to OpenStreetMap’s Nominatim API to get your exact
coordinates unless the coordinates are specified.

`latitude` and `longitude` are your exact coordinates; the more accurate the
better. These will be sent to OpenStreeMap’s Nominatim API to get your country
and city unless they are specified in your config.  They will also be used to
find the time zone you live in, unless you have that specified in your config.

`timezone` is the time zone you live your life in.

Output
======

To show the textual output, use ``poetry run print_data``.

The output is currently limited to the terminal only (ie. there’s no graphical
application or anything to display it.

```
Now: 2022-03-24 08:18:24 (UTC 2022-03-24 07:18:24)
Morning, 3:30:50.393675 until Noon
Next Rāhukāla at 12:22:20
Waning Gibbous Moon
rainbow hour (rainbow, 7)
```

The first row is the current date and time both in your timezone set, and in
UTC (Universal Coordinated Time).

The second row shows the current part of the day, which can be:

- Night
- Blue hour (when the Sun is just below the horizon)
- Golden hour (when the Sun is just above the horizon)
- Morning (after the Golden hour until noon)
- Noon (which is actually not 12:00 more often than not)
- Afternoon
- Golden hour (when the Sun is still just above the horizon)
- Blue hour (when the Sun is already just below the horizon)

The third row tells you if you are in the period of
[Răhukăla](https://en.wikipedia.org/wiki/R%C4%81huk%C4%81la) a part of the day
when it’s not favourable to start a good deed, based on the Vedic philosophy.

The fourth row shows you the current phase of the Moon.

The fifth row shows you the seasonal hour.

OK, but what is this all about?
===============================

To quote the original idea:

> It would be nice if the 24 hours of UTC time each had a short memorable name.
> It would make it easier to plan chats with distant friends, since the
> hour-names would be synchronized around the world.

> Let's choose a theme like... seasons of the year, just to be confusing. :)
> Squish a year into 24 hours starting with the winter solstice at UTC 00, which
> we'll call The Candle Hour.

It can theoretically make it easier to synchronize events across multiple time
zones without actually knowing the time difference; just tell the other
attending parties that you eat your lunch during Ladybug hour, or that your
event is taking place between Gourd and Soup hours.

Generated SVG
=============

With the ``poetry run print_svg`` command, you will get an image similar to this:

.. image:: example-output.svg
   :width: 700
   :alt: An image rendered by this software
